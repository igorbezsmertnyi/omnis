const userToken = token => JSON.stringify({ session_token: token })

module.exports = userToken
